#-*-coding:utf-8-*-

import re

class Motsstopliste:
    def __init__(self, chemin, encodage):
        self.encodage = encodage
        self.chemin = chemin
        self.stopwordliste = self.installationstopliste()
        
    def installationstopliste(self):
        f = open(self.chemin, 'r',encoding=self.encodage)
        texte=f.read()
        f.close()
        
        patternTexte = re.compile('[\ufeff;:.,!?\»\« \n\']+')
        self.stopwordliste = patternTexte.split(texte.lower())
        while '' in self.stopwordliste:
            self.stopwordliste.remove('')
        return self.stopwordliste
        
        