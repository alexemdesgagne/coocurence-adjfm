#-*-coding:utf-8-*-
import sys
import cx_Oracle
from infosConnect import Information

PATH_ORACLE = 'C:\Oracle\Client\product\12.2.0\client_1\bin'
sys.path.append(PATH_ORACLE)
infos = Information()
dsn_tns = cx_Oracle.makedsn(infos.host, 1521, infos.sid)
chaineConnexion = infos.usr_name + '/' + infos.passwd + '@' + dsn_tns

class Connexion:
    def __init__(self):
        self.connexion = cx_Oracle.connect(chaineConnexion)
        self.curseur = self.connexion.cursor()
        
    def fermetureconnexion(self):
        self.curseur.close()
        self.connexion.close()

#importationMotBD
    def importationMotBD(self):
        rangee = dict(self.curseur.execute('SELECT nom, position_mot FROM mot').fetchall())
        return rangee

    def lectureBDMot(self):
        rangee = self.curseur.execute('SELECT nom, position_mot FROM mot').fetchall()
        return rangee

#recherche des liaisons existante dans la base de données    
    def creationDictUpdate(self,tailleFenetre):
        dictUpdate={}
        enonce = 'SELECT * FROM liaison_mots WHERE tailleFenetre = :1'
        params=[(tailleFenetre)]
        resultat = self.curseur.execute(enonce, params)
        resultat =  resultat.fetchall()        
        #resultat est une liste de tuple
        #creer le dictionnaire ayant comme clé un tuple
        for li in resultat:
            dictUpdate[(li[0],li[1],li[2])] = li[3]
        return dictUpdate

    def lectureBDLiaisons(self):
        dictUpdate={}
        enonce = 'SELECT * FROM liaison_mots'
        resultat = self.curseur.execute(enonce)
        resultat =  resultat.fetchall()        
        return resultat




