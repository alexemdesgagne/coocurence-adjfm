#-*-coding:utf-8-*-
import time
import re
from connexion import Connexion
import numpy as np
import math as m

class Entrainement:
#__init__
    def __init__(self,encodage,chemin,tailleFenetre):
        self.encodage = encodage
        self.chemin = chemin
        self.tailleFenetre = tailleFenetre
        self.texte = self.lectureTexte(self.chemin,self.encodage)
        self.listeMots=self.parseText(self.texte)
        self.dictMots = self.creationDict(self.listeMots)
        self.connexion = self.connexionbasededonnee()
        self.ajoutmotsbasededonnee() #Mise a jour de la liste de mots dans
        self.dictUpdate = self.connexion.creationDictUpdate(tailleFenetre) #dictionnaire de tuple pour les updates
        self.dictUpdateAUpdater = {} #dictionnaire pour seulement updater les liaisons qui ont été modifier
        self.dictInsertion = self.fenetre(self.listeMots)
        self.updateLiaisons()
        self.insertionLiaisons()
        self.connexion.fermetureconnexion()#fermeture de la base de données

#connexionbasededonnee   
    def connexionbasededonnee(self):
        connexion = Connexion()
        return connexion

#lectureTexte   
    def lectureTexte(self, chemin, encodage):
        texte = ""
        
        try:
            f = open(chemin, 'r', encoding = encodage) #ouverture du fichier
            
            texte = f.read()
            f.close() #fermeture du fichier
        except:
            #problème de lecture
            print('\nImpossible de lire {0}'.format(chemin))
            return 0
        return texte

#parseText
    def parseText(self, texte):
        listeMots = []
        pattern = re.compile( "[^\W0123456789\ufeff]+") #expression régulière
        listeMots += re.findall(pattern,str(texte).lower()) #extraction des mots
        return listeMots

#createDict
    def creationDict(self,listeMots): #création du dictionnaire qui contient les positions des mots
        dictMots = {}
        for mots in listeMots:
            if mots not in dictMots:
                dictMots[mots] = None	
        return dictMots

#ajoutsbasededeonnee       
    def ajoutmotsbasededonnee(self):
        rangeeBD = self.connexion.importationMotBD()
        listeAInserer = []
        dictAInserer = {}
        for mot in self.dictMots:
            if mot not in rangeeBD:
                dictAInserer[mot] = None
        listeAInserer = list(dictAInserer.items())
        enonce = 'INSERT INTO mot VALUES(:1,:2)'
        self.connexion.curseur.executemany(enonce, listeAInserer)
        self.connexion.connexion.commit()    

#fenetre 
    def fenetre(self,listeMots): #fenetre permet de remplir la matrice de coocurences
        dictEntrainement = {}
        indiceDepart = m.floor(self.tailleFenetre/2)
        tailleChaqueCote = indiceDepart
        indiceCourant = indiceDepart
        for j in range(indiceDepart,len(listeMots)-tailleChaqueCote): #du début à la fin de la liste de mots
            indiceCourant = j 
            mot1 = listeMots[indiceCourant]
            for i in range(1,tailleChaqueCote+1): #chaques mots autour du mot central
                for val in range(-1,2,2): #mot avant et après
                    mot2 = listeMots[indiceCourant+(i*val)]  
                    tupleMot = (mot1,mot2,self.tailleFenetre) #Création de la clée
                    if tupleMot in self.dictUpdate:
                        self.dictUpdateAUpdater[tupleMot] = self.dictUpdate[tupleMot]+1
                        self.dictUpdate[tupleMot] += 1
                    else: 
                        if tupleMot in dictEntrainement:
                            dictEntrainement[tupleMot] += 1
                        else:
                            dictEntrainement[tupleMot] = 1
        return dictEntrainement

#Update des anciennes liaisons venant du dictionnaire dictUpdate
    def updateLiaisons(self):
        listBD = []

        for (mot1, mot2, tailleFenetre), valeur in self.dictUpdateAUpdater.items():
            listBD.append((valeur, mot1, mot2, tailleFenetre))   
        enonce = 'UPDATE liaison_mots SET resultatCoo = :1  WHERE mot1 = :2 AND mot2 = :3 AND tailleFenetre = :4'

        self.connexion.curseur.executemany(enonce, listBD)
        self.connexion.connexion.commit()

#Insertion des nouvelles liaisons dans la base de données selon la taille de fenêtre de l'entrainement
    def insertionLiaisons(self):
        listBD = []

        for (mot1, mot2, tailleFenetre), valeur in self.dictInsertion.items():
            listBD.append((mot1, mot2, tailleFenetre, valeur))

        enonce = 'INSERT INTO liaison_mots VALUES (:1,:2,:3,:4)'
        self.connexion.curseur.executemany(enonce, listBD)
        self.connexion.connexion.commit()
