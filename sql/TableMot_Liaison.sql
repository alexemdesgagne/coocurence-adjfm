SET ECHO OFF

-- =======================================================
PROMPT DESTRUCTION DES TABLES
-- =======================================================

DROP TABLE liaison_mots;
DROP TABLE mot;
DROP SEQUENCE seq_inc_position;
DROP TRIGGER trigger_inc_position;
DROP INDEX idx_liaison;

-- =======================================================
PROMPT CR�ATION DES S�QUENCES
-- =======================================================

CREATE SEQUENCE seq_inc_position
MINVALUE 0
START WITH 0
INCREMENT BY 1;

-- =======================================================
PROMPT CR�ATION DES TABLES
-- =======================================================

CREATE TABLE mot(
	nom VARCHAR2(100),
	position_mot NUMBER NOT NULL,
	CONSTRAINT pk_mot PRIMARY KEY(nom)
);

CREATE TABLE liaison_mots(
	mot1 VARCHAR2(100),
	mot2 VARCHAR2(100),
	tailleFenetre NUMBER(2) NOT NULL,
	resultatCoo NUMBER DEFAULT 0,
    CONSTRAINT pk_liaison_mots PRIMARY KEY(mot1, mot2, tailleFenetre),
	CONSTRAINT fk_mot1 FOREIGN KEY(mot1) REFERENCES mot(nom),
	CONSTRAINT fk_mot2 FOREIGN KEY(mot2) REFERENCES mot(nom)
);

-- =======================================================
PROMPT CR�ATION DES TRIGGER
-- =======================================================

--TRIGGER pour ajouter une position au mot.
CREATE OR REPLACE TRIGGER trigger_inc_position
  BEFORE INSERT ON mot  
  FOR EACH ROW
BEGIN
  SELECT seq_inc_position.nextval
  INTO :new.position_mot
  FROM dual;
END;
/
COMMIT;
