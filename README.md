# README #

### Membres de l'équipe ###
* Jean-François Meehan
* Alexandre Desgagné

###/sql/TableMot_Liaison.sql###
Fichier sql pour la base de données.

### Spécifications pour le fichier infosConnect.py  ###
Créez-vous un fichier infosConnect.py dans le même répertoire que connexion.py avant d'ouvrir le programme
À insérer à l'intérieur: 

class Information:
    def __init__(self):
        self.usr_name = 'nom utilisateur'
        self.passwd = 'mot de passe'
        self.host = 'hôte'
        self.sid = 'sid'


### Excellente recette de gâteau au chocolat ###

lien : https://www.ricardocuisine.com/recettes/6151-le-meilleur-meilleur-gateau-au-chocolat

Le meilleur­meilleur gâteau au chocolat
Préparation
Cuisson
Refroidissement
Rendement
Se congèle
45 MIN
50 MIN
1 H
8 à 10 portions
Ingrédients
Gâteau
Ganache
300 g (2 tasses) de farine tout usage non blanchie
5 ml (1 c. à thé) de bicarbonate de soude
5 ml (1 c. à thé) de poudre à pâte
2,5 ml (1/2 c. à thé) de sel
2 oeufs
420 g (2 tasses) de sucre
250 ml (1 tasse) de lait de beurre
125 ml (1/2 tasse) d’huile de canola
10 ml (2 c. à thé) d’extrait de vanille
75 g (3/4 tasse) de cacao
180 ml (3/4 tasse) de café corsé, tiède
450 g (16 oz) de chocolat noir, haché
430 ml (1 3/4 tasse) de crème 35 %
30 ml (2 c. à soupe) de sirop de maïs clair
Préparation
1. Placer la grille au centre du four. Préchauffer le four à 180 °C (350 °F). Beurrer les parois et tapisser deux moules à charnière
de 20 cm (8 po) de diamètre de papier parchemin.
2. Dans un bol, mélanger la farine, le bicarbonate, la poudre à pâte et le sel. Réserver.
3. Dans un autre bol, mélanger les oeufs, le sucre, le lait de beurre, l’huile et la vanille au fouet. Réserver.
4. Dans un petit bol, mélanger le cacao et le café au fouet. Ajouter au mélange d’oeufs. Incorporer les ingrédients secs et
mélanger jusqu’à ce que la pâte soit homogène. Répartir la pâte dans les moules.
5. Cuire au four de 45 à 50 minutes ou jusqu’à ce qu’un cure­dent inséré au centre du gâteau en ressorte propre. Laisser refroidir
complètement sur une grille. Démouler.
Ganache
6. Placer le chocolat dans un bol.
7. Dans une petite casserole, porter à ébullition la crème et le sirop de maïs. Verser sur le chocolat. Laisser reposer 2 minutes
avant de remuer.
