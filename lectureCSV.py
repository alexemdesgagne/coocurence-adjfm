#-*-coding:utf-8-*-

import csv
import sys


class LectureFichierCSV:
    def __init__(self):
        self.dictionnaireMot = self.lectureMotCSV()
        self.dictionnaireLiaisons = self.lectureLiaisonsCSV()

    def lectureMotCSV(self):
        dicttemp={}

        f = open('mots.csv', 'rt',encoding = 'utf-8')
        try:
            reader=csv.reader(f)
            for row in reader:
                dicttemp[row[0]] = row[1]
        finally:
            f.close()
        return dicttemp

    def lectureLiaisonsCSV(self):
        dicttemp={}
        f = open('liaisons.csv', 'rt',encoding = 'utf-8')
        try:
            reader=csv.reader(f)
            for row in reader:
                dicttemp[(row[0], row[1], row[2])] = row[3]        
        finally:
            f.close()
        return dicttemp