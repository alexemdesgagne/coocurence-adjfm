import numpy as np
import random
import numpy as np
from motstopliste import Motsstopliste

class TravailCluster:
    def __init__(self, nbCentroide,dictMots,dictLiaisons,listeMots):
        self.nbCentroide = nbCentroide
        self.dictMots = dictMots
        self.dictLiaisons = dictLiaisons
        self.nbMots = len(self.dictMots)
        self.matrice =  self.creationMatrice()
        self.listeCentroides = self.centroideDepart(listeMots)
        self.listeAppartenance = self.initListeAppartenance()
        self.decompte = self.initialiserDecompte()
        self.additionMots = self.initialiserAdditionMots() # addition des vecteurs de résultats des mots pour les futurs centroides

    def centroideDepart(self, listeMots):
        
        listeCentroides = []
        if len(listeMots) != 0:
            for mot in listeMots:
                if mot in self.dictMots:
                    listeCentroides.append(self.matrice[int(self.dictMots[mot])])
                else:
                    idxMot = random.randint(0, self.nbMots) -1
                    listeCentroides.append(self.matrice[idxMot])
        else:#centroide aléatoire
            for i in range(0,self.nbCentroide):
                #creer matrice linéaire selon le nombre de mots
                matricetemp = np.zeros(self.nbMots)
                for j in range(0, self.nbMots):
                    valeurtemp = random.randint(0, self.nbMots)-1
                    matricetemp[j] = self.matrice[i][valeurtemp]
                listeCentroides.append(matricetemp)                
        return listeCentroides
                    
            
        '''
        if len(listeMots) != 0:
            for mot in listeMots:
                if mot in self.dictMots:
                    listeCentroides.append(self.matrice[int(self.dictMots[mot])])
                else:
                    idxMot = random.randint(0, self.nbMots) -1
                    listeCentroides.append(self.matrice[idxMot])
        else:
            minimum = np.argmin(self.matrice)
            maximum = np.argmax(self.matrice)
            for i in range(0,self.nbCentroide):
                #idxMot = random.randint(0, self.nbMots) -1
                #listeCentroides.append(self.matrice[idxMot])              
                centroide = np.random.randint(minimum,maximum,len(self.dictMots)) 
                listeCentroides.append(centroide)
        return listeCentroides
        '''
      
    def creationMatrice(self):
        matrice = np.zeros((self.nbMots,self.nbMots)) #Création de la matrice de coocurences.
        for liaison in self.dictLiaisons:
            valeur = self.dictLiaisons[liaison]
            posMot1 = self.dictMots[liaison[0]]
            posMot2 = self.dictMots[liaison[1]]
            matrice[int(posMot1)][int(posMot2)] = valeur
        return matrice

    def remplirListeAppartenance(self): 
        nbChangements = 0 
        idx = 0
        for mot in self.dictMots:
            vecteurMot = self.matrice[int(self.dictMots[mot])]
            distanceMin = 0
            distance = 0
            for i in range(0,len(self.listeCentroides)):
                vecteurCentroide =  self.listeCentroides[i]
                distance = np.sum((vecteurCentroide-vecteurMot)**2)
                if(i == 0):
                    distanceMin = distance
                    idx = i
                if(distance < distanceMin):
                    distanceMin = distance
                    idx = i
            if idx != self.listeAppartenance[int(self.dictMots[mot])]: #Si on change de cluster.
                nbChangements += 1
            self.listeAppartenance[int(self.dictMots[mot])] = idx
        return nbChangements

    def initListeAppartenance(self):
        listeAppartenance = []
        for i in range(0,self.nbMots):
            listeAppartenance.append(-1)
        return listeAppartenance

    def initialiserDecompte(self):
        listetemp = np.zeros((self.nbCentroide,1))
        return listetemp

    def initialiserAdditionMots(self):
        listetemp = np.zeros((self.nbCentroide, len(self.dictMots) ))
        return listetemp

    def miseAJourListeCentroides(self):
        listetemp = []
        listetemp = np.zeros((self.nbCentroide, len(self.dictMots) ))
        for i in range (0,self.nbCentroide):
            if(self.decompte[i]!=0):
                arraytemp = (  self.additionMots[i] / self.decompte[i]  )
            else:
                arraytemp = (  self.additionMots[i] / 1 )
            listetemp[i] = np.array(arraytemp)      
        #Réinitialiser listeCentroides et additionMots
        self.additionMots = self.initialiserAdditionMots()
        self.decompte = self.initialiserDecompte()
        return listetemp

    def calculerNouveauCentroide(self):
        for mot in self.dictMots:
            matrice = self.matrice[int(self.dictMots[mot])]
            indiceCentroide = self.listeAppartenance[int(self.dictMots[mot])]
            self.additionMots[indiceCentroide] = self.additionMots[indiceCentroide] + matrice
            self.decompte[indiceCentroide] += 1
            decompteTemp = self.decompte
        self.listeCentroides = self.miseAJourListeCentroides() 
        return decompteTemp

    def retournerResultat(self):
        chemin='stopwords_Liste.txt'
        stopList = Motsstopliste(chemin, 'utf-8')
        stopwordliste = stopList.stopwordliste

        dictMot2 = {}
        listeParCentroide=[]
        listeTotal=[]

        for key, value in self.dictMots.items():
            dictMot2[str(value)] = key

        for i in range(self.nbCentroide):
            listeTotal.append([])

        for i in range(0,len(self.listeAppartenance)):
            if dictMot2[str(i)] not in stopwordliste:
                matriceCentroide = self.listeCentroides[int(self.listeAppartenance[i])]
                matriceMot = self.matrice[i]
                resultat = np.sum(  (matriceMot-matriceCentroide)**2  )
                tupleres = (dictMot2[str(i)], resultat)
                listeTotal[int(self.listeAppartenance[i])].append(tupleres)
        return listeTotal
    