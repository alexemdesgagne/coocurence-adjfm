#-*-coding:utf-8-*-

from connexion import Connexion
import csv
import sys

class EcritureFichierCSV:
    def __init__(self):
        self.connexion = Connexion()
        self.BDmot = self.connexion.lectureBDMot()
        self.BDLiaisons = self.connexion.lectureBDLiaisons()
        self.connexion.fermetureconnexion()
        self.ecritureCSVMot()
        self.ecritureCSVLiaisons()

    def ecritureCSVMot(self):
        f = open('mots.csv', 'wt', newline='', encoding = 'utf-8')
        try:
            writer = csv.writer(f)
            writer.writerows(self.BDmot)
        finally:
            f.close()

    def ecritureCSVLiaisons(self):
        f = open('liaisons.csv', 'wt', newline='', encoding = 'utf-8')
        try:
            writer = csv.writer(f)
            writer.writerows(self.BDLiaisons)
        finally:
            f.close()

