#-*-coding:utf-8-*-

def analyseCommande(arg):
    arguments = {}
    try:
        if "-s" in arg: 
            if "-e" not in arg and "-t" in arg and len(arg) == 4:
                arguments["-s"] = 0
                arguments["-t"] = int(arg[arg.index("-t")+1])
            else:
                raise Exception()
        elif "-e" in arg:
            if "-s" not in arg and "-cc" in arg and "-enc" in arg and "-t" in arg and len(arg) == 8:
                arguments["-e"] = 0
                arguments["-t"] = int(arg[arg.index("-t")+1])
                arguments["-cc"] = str(arg[arg.index("-cc")+1])
                arguments["-enc"] = str(arg[arg.index("-enc")+1])
            else:
                raise Exception()
        elif "-csv" in arg and len(arg) == 2: #Bons arguments
            arguments["-csv"] = 0
        elif "-nc" in arg and len(arg) == 7:
            if "-t" in arg and "-n" in arg:
                arguments["-nc"] = int(arg[arg.index("-nc")+1])   #Nombre de centroïdes
                arguments["-t"] = int(arg[arg.index("-t")+1])   #Taille de la fenêtre
                arguments["-n"] = int(arg[arg.index("-n")+1])   #Nombre de mot voulus
            else:
                raise Exception()
        elif "-m" in arg and len(arg) == 7:
            if "-t" in arg and "-n" in arg:
                arguments["-t"] = int(arg[arg.index("-t")+1])   #Taille de la fenêtre
                arguments["-n"] = int(arg[arg.index("-n")+1])   #Nombre de mot voulus
                arguments["-m"] = str(arg[arg.index("-m")+1])   #chaine pour les mots.
        else:
            raise Exception()
        return arguments
    except:
        print("Erreur dans votre commande.")
        return 0

def analyseDemande(demande):
    infos = []
    listeDemande = demande.split() 	
    try:
        #traitement de la demande de l'usager
        if len(listeDemande) != 3:
            raise Exception() #problème
        mot = listeDemande[0]
        nombreMots = int(listeDemande[1])
        fonctionScore = int(listeDemande[2]) 
        infos.append(mot)
        infos.append(nombreMots)
        infos.append(fonctionScore)        
        if fonctionScore != 0 and fonctionScore != 1 and fonctionScore != 2:
            raise Exception() #problème
    except:
        print("\nErreur dans votre demmande.")
        return 0
    return infos