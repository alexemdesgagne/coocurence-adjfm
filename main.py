#-*-coding:utf-8-*-
import sys
import time
from entrainement import *
from chercheur import *
from traitementUsr import *
from ecritureCSV import EcritureFichierCSV
from lectureCSV import *
from travailCluster import *

def main():
    arguments = analyseCommande(sys.argv) 
    if arguments != 0:
        if "-e" in arguments:
            entrainement = Entrainement(arguments["-enc"],arguments["-cc"],arguments["-t"])
        elif "-s" in arguments:
            chercheur = Chercheur(arguments["-t"])
            print("\nEntrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul")
            print("i.e. produit scalaire: 0   least squares: 1   city block: 2")
            demande = input("\nTapez -1 pour quitter ")
            while demande != '-1':
                infos = []
                infos = analyseDemande(demande)
                if infos != 0:
                    listeResultat = chercheur.rechercheMot(infos)
                    affichageResultat(listeResultat, infos[1])
                print("\nEntrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul")
                print("i.e. produit scalaire: 0   least squares: 1   city block: 2")
                demande = input("\nTapez -1 pour quitter ")
        elif "-csv" in arguments:     
            EcritureFichierCSV()
            print("Transfert des données terminé.")
        elif "-nc" in arguments or "-m" in arguments:
            listesResultats = []
            listeMots = []
            nbCluster = 0
            start = time.time()
            iteration = 1
            l = LectureFichierCSV()
            if "-m" in arguments:
                liste = arguments["-m"]
                listeMots = liste.split()
                nbCluster = len(listeMots)       
            elif "-nc" in arguments:
                nbCluster = arguments["-nc"]   
            tc = TravailCluster(nbCluster,l.dictionnaireMot,l.dictionnaireLiaisons,listeMots)
            nbChangements = tc.remplirListeAppartenance()
            decompte = tc.calculerNouveauCentroide()
            affichageInfosIter(iteration,decompte,nbChangements)
            while nbChangements != 0:
                iteration += 1
                nbChangements = tc.remplirListeAppartenance()
                decompte = tc.calculerNouveauCentroide()
                affichageInfosIter(iteration,decompte,nbChangements)
                end = time.time()
            listesResultats = tc.retournerResultat()
            iteration = 1
            for liste in listesResultats:
                print('\nMots dans le groupe {}: '.format(iteration))
                affichageResultat(liste, arguments["-n"])
                iteration += 1
            print('\nTemps exécution: {}'.format(time.strftime("%H:%M:%S", time.gmtime(end-start))))
			

def affichageInfosIter(iteration,decompte,nbChangements):
    print('Itération numéro: {}'.format(iteration))
    for i in range(0, len(decompte)):
        print('Nombre de mot dans le cluster {0}: {1}'.format(i,decompte[i][0]))
    print('Nombre de changements dans les clusters: {}\n'.format(nbChangements))
    
def affichageResultat(listeResult, nbresult):
    listeResult = sorted(listeResult, key=lambda x: x[1], reverse=False)
    for i in range (nbresult+1):
        if i < len(listeResult):
            if i < 10:
                print(i, '  : ', listeResult[i][0], ', ', listeResult[i][1] )
            else:
                print(i, ' : ', listeResult[i][0], ', ', listeResult[i][1] )
        else:
            return

if __name__ == "__main__":
    SystemExit(main())
