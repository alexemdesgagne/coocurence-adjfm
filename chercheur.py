#-*-coding:utf-8-*-

import numpy as np
from motstopliste import *
from connexion import Connexion

class Chercheur:
    def __init__(self,tailleFenetre):
        self.fonctionCalcul = None
        self.tailleFenetre = tailleFenetre
        self.motstopliste = Motsstopliste("stopwords_Liste.txt","utf8")
        self.stopList = self.motstopliste.stopwordliste
        self.connexion = self.connexionBD()
        self.motBD = self.connexion.importationMotBD()
        self.matrice =  self.creationMatrice()
        self.connexion.fermetureconnexion()

    def connexionBD(self):
        connexion = Connexion()
        return connexion

    def calculLeastSquares(self, vecteurMotChoisi, vecteurTemp):
        self.triOrdre = False
        return np.sum(  (vecteurMotChoisi-vecteurTemp)**2  ) 
        
    def calculProduitsScalaires(self, vecteurMotChoisi, vecteurTemp):
        self.triOrdre = True
        return np.sum(vecteurMotChoisi * vecteurTemp)
        
    def calculCityBlock(self,vecteurMotChoisi, vecteurTemp):
        self.triOrdre = False
        return np.sum(np.absolute( vecteurMotChoisi- vecteurTemp)) 

    def setFonctionCalcul(self, facon):
            if facon == 0:
                self.fonctionCalcul = self.calculProduitsScalaires
            elif facon == 1:
                self.fonctionCalcul = self.calculLeastSquares
            else: 
                self.fonctionCalcul = self.calculCityBlock
            
    def creationMatrice(self):
        matrice = np.zeros((len(self.motBD),len(self.motBD))) #Création de la matrice de coocurences.
        dictLiaisons = self.connexion.creationDictUpdate(self.tailleFenetre)  #Importation des données de coocurence de la BD
        for liaison in dictLiaisons:
            valeur = dictLiaisons[liaison]
            posMot1 = self.motBD[liaison[0]]
            posMot2 = self.motBD[liaison[1]]
            matrice[posMot1][posMot2] = valeur
        return matrice
            
    def rechercheMot(self, infos):
        #infos[0] = mot, infos[1] = nombreMots, infos[2] = fonctionScore 
        self.setFonctionCalcul(infos[2])
        self.motrechercher = infos[0]

        vecteurMotChoisi = self.matrice[ self.motBD[self.motrechercher] ]
        listResult = []
        
        for mot in self.motBD:
            if mot != self.motrechercher and mot not in self.motstopliste.stopwordliste:
                #faire calcul des matrice
                vecteurTemp = self.matrice[   self.motBD[mot]   ]
                resultatScalaire = self.fonctionCalcul(vecteurMotChoisi, vecteurTemp)
            
                #entrer le resultat dans la liste avec le mot
                listAppend = (mot,resultatScalaire)
                listResult.append(listAppend)         
            #trier la liste avant de la retourner
        listResult = sorted(listResult, key=lambda x: x[1], reverse=self.triOrdre)
        return listResult


