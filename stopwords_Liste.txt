﻿à, demi, peine, peu près, absolument, actuellement, ainsi, alors, apparemment, approximativement, après, après-demain, assez, assurément, au, aucun, aucunement, aucuns, aujourd’hui, auparavant, aussi, aussitôt, autant, autre, autrefois, autrement, avant, avant-hier, avec, avoir
beaucoup, bien, bientôt, bon
c’, ça, car, carrément, ce, cela, cependant, certainement, certes, ces, ceux, chaque, ci, comme, comment, complètement
d’, d’abord, dans, davantage, de, début, dedans, dehors, déjà, demain, depuis, derechef, des, désormais, deux, devrait, diablement, divinement, doit, donc, dorénavant, dos, droite, drôlement, du
elle, elles, en, en vérité, encore, enfin, ensuite, entièrement, entre-temps, environ, essai, est, et, étaient, état, été, étions, être, eu, extrêmement
fait, faites, fois, font, force
grandement, guère
habituellement, haut, hier, hors
ici, il, ils, infiniment, insuffisamment
jadis, jamais, je, joliment
ka, dont, y, avaient, toutes, autres, entre
la, là, le, les, leur, leurs, lol, longtemps, lors
ma, maintenant, mais, mdr, même, mes, moins, mon, mot
naguère ,ne, ni, nommés, non, notre, nous, nouveaux, nullement
ou, où, oui, uns, aux
par, parce, parfois, parole, pas, pas mal, passablement, personne, personnes, peu, peut, peut-être, pièce, plupart, plus, plutôt, point, pour, pourquoi, précisément, premièrement, presque, probablement, prou, puis
quand, quasi, quasiment, que, quel, quelle, quelles, quelque, quelquefois, quels, qui, quoi, quotidiennement
rien, rudement, ont, auraient, devaient, venaient, faisaient, allaient, eurent, furent, eussent, ayant, voulaient
s’, sa, sans, sans doute, ses, seulement, si, sien, sitôt, soit, son, sont, soudain, sous, souvent, soyez, subitement, suffisamment, sur
t’, ta, tandis, tant, tantôt, tard, tellement, tellement, tels, terriblement, tes, ton, tôt, totalement, toujours, tous, tout, tout à fait, toutefois, très, trop, tu
un, une
valeur, vers, voie, voient, volontiers, vont, votre, vous, vraiment, vraisemblablement
'le', 'la', 'de', 'et', 'l', '--', 'd','que', 'je', 'tu', 'il', \
                               'nous', 'vous', 'ils', 'que', 'un', 'une', 'les','en', 'qui', 'm', 'son', \
                               'ce', 'pas', 'des', 'se', 's', 'qu', 'lui', 'du', 'du', 'dans', 'sa', 'sa', \
                               'au', 'ne', 'pour', 'cette', 'mais', 'sur', 'ses', 'plus', 'tout', 'deux','avec', \
                               'comme', 'me','elle','elles', 'on', 'par', 'n', 'c', 'votre', 'dit', 'était', \
                               'est', 'avait', 'a', 'si','mon','ai', 'j', 'faire'